# Kotlin_Simple_API

I wrote this as a way of experimenting with Spring boot/JDBC integration with 
kotlin. As an API it does absolutely nothing of any importance. 

Also I added some leetcode solutions to keep general programming skills sharp, but
left them in here with API calls to find the solution for some reason. 