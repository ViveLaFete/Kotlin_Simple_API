CREATE TABLE IF NOT EXISTS people (
    age integer,
    id INTEGER PRIMARY KEY NOT NULL,
    name text NOT NULL,
    Nationality text
);