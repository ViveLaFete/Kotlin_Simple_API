package controllers

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * @property age The age of person
 * @property name the name of the person
 * @property nationality The nationality of the user
 */
data class Person @JsonCreator constructor(
        @JsonProperty("age")
        val age: Int,
        @JsonProperty("name")
        val name: String,
        @JsonProperty("nationality")
        val nationality: String
)

data class Pets @JsonCreator constructor(
        @JsonProperty("name")
        val name: String,

        @JsonProperty("breed")
        val breedId: Int
)
