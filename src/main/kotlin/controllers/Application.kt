package controllers

import data.PersonDatabase
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

//essentially a public static main method

@SpringBootApplication
class Application

fun main(args: Array<String>) {
    val userDir: String = System.getProperty("user.dir")
    val db = PersonDatabase()
    db.initialiseDatabase("$userDir/SQL/people.sql")
    SpringApplication.run(Application::class.java, *args)
}