package controllers

import data.PersonDatabase
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.ThreadLocalRandom

@RestController
class PersonController {

    @DeleteMapping("/removePerson")
    fun deletePerson(@RequestParam("id") id: Int) {
        val dbRef = PersonDatabase()
        dbRef.deleteUser(id)
    }

    @PostMapping("/addPerson")
    fun putPerson(@RequestBody person: Person) {
        val postgresDB = PersonDatabase()
        postgresDB.insertPerson(person.age, person.name,
                ThreadLocalRandom.current().nextInt(), person.nationality)
    }

    @GetMapping("/getAllUsers")
    fun getAllUsers(): List<Person> {
        val postgresDB = PersonDatabase()
        return postgresDB.getAllPeople()
    }
}