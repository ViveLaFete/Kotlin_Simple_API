package controllers

import LeetCodeSolutions.frequencySort
import LeetCodeSolutions.predictTheWinner
import data.PredictWinner
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class LeetCodeController {

    @PostMapping("/leetcode/getPredictWinner")
    fun getPredictAnswerRes(@RequestBody predict: PredictWinner): String {
        return "For input " + predict.values + ": " + predictTheWinner(predict.values)
    }

    @PostMapping("/leetcode/getSortedStringByInstanceOfCharacter")
    fun getSortedStringByInstanceOfCharacter(@RequestParam("input") input : String): String {
        return frequencySort(input)
    }
}