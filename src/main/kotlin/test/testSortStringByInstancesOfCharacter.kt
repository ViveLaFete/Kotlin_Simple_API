package test

import LeetCodeSolutions.frequencySort
import org.junit.Assert
import org.junit.Test

class testSortStringByInstancesOfCharacter {

    @Test
    fun testEmptyString() {
        val res = frequencySort("")
        Assert.assertEquals(res, "")
    }

    @Test
    fun testSimple() {
        val input = "hello"
        val res = frequencySort(input)
        Assert.assertEquals(res, "llheo")
    }

    @Test
    fun testOddInput() {
        val input = "décisif"
        val res = frequencySort(input)
        Assert.assertEquals(res, "iidécsf")
    }
}