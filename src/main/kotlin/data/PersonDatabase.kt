package data

import controllers.Person
import org.json.JSONObject
import java.io.File
import java.io.InputStream
import java.sql.*

/**
 * Will connect to a postgresSQL database and write stuff
 */
class PersonDatabase {

    private val dataBaseConfigData: DB_CONFIG = DB_CONFIG()
    private val DATABASE_LOCATION = "jdbc:postgresql://localhost:5432/" + dataBaseConfigData.DB_NAME
    private val USERNAME = dataBaseConfigData.username
    private val PASSWORD = dataBaseConfigData.password

    fun initialiseDatabase(pathToSchema: String) {
        val tableCommand: String = getFileAsString(pathToSchema)
        val conn: Connection = makeConnection(DATABASE_LOCATION, USERNAME, PASSWORD)
        val statement: Statement? = conn.createStatement()
        statement?.execute(tableCommand)
        conn.close()
    }

    fun insertPerson(age: Int, name: String, id: Int, nationality: String) {
        val insertPerson = "INSERT INTO people(age,id,name,nationality) VALUES(?,?,?,?)"
        val conn: Connection = makeConnection(DATABASE_LOCATION, USERNAME, PASSWORD)
        val ps: PreparedStatement = conn.prepareStatement(insertPerson).apply {
            setInt(1, age)
            setInt(2, id)
            setString(3, name)
            setString(4, nationality)
        }
        ps.executeUpdate()
        conn.close()
    }

    fun getAllPeople(): List<Person> {
        val getAllPeps = "SELECT * FROM people"
        val conn: Connection = makeConnection(DATABASE_LOCATION, USERNAME, PASSWORD)
        val results: ResultSet? = conn.createStatement().executeQuery(getAllPeps)
        val allPeople: MutableList<Person> = ArrayList()
        if (results != null) {
            while (results.next()) {
                val person = Person(
                        results.getInt("age"),
                        results.getString("name"),
                        results.getString("nationality")
                )
                allPeople.add(person)
            }
        }
        conn.close()
        return allPeople
    }

    fun deleteUser(id: Int) {
        val deletePerson = "DELETE FROM people WHERE id = ?"
        val conn: Connection = makeConnection(DATABASE_LOCATION, USERNAME, PASSWORD)
        val preparedStatement: PreparedStatement = conn.prepareStatement(deletePerson)
        preparedStatement.setInt(1, id)
        preparedStatement.executeUpdate()
    }

    private fun makeConnection(url: String, userName: String, password: String): Connection {
        return DriverManager.getConnection(url, userName, password)
    }

    private fun getFileAsString(pathToFile: String): String {
        val input: InputStream = File(pathToFile).inputStream()
        return input.bufferedReader().use { it.readText() }
    }
}