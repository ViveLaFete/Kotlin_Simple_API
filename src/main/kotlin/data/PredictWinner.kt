package data

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class PredictWinner @JsonCreator constructor(
        @JsonProperty("values")
        val values: List<Int>
)