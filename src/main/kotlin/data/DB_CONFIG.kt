package data

data class DB_CONFIG(
        val username: String = "postgres",
        val password: String = "postgres",
        val DB_NAME: String = "mydb"
)