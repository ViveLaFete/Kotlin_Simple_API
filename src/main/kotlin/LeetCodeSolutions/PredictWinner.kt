package LeetCodeSolutions

fun predictTheWinner(nums: List<Int>) : Boolean {
    fun deriveAns(minInd: Int, maxIndex: Int): Int {
        return if (minInd == maxIndex) {
            nums[minInd]
        } else {
            Math.max(nums[maxIndex] - deriveAns(minInd, maxIndex - 1),
                    nums[minInd] - deriveAns(minInd + 1, maxIndex))
        }
    }

    val memoisedMutArray: Array<IntArray> = Array(5) { IntArray(5) }
    fun deriveAnsMemo(minInd: Int, maxIndex: Int, memo: Array<IntArray>): Int {
        if (memo[minInd][maxIndex] == 0) {
            if (minInd == maxIndex) {
                memo[minInd][maxIndex] = nums[minInd]
            } else {
                memo[minInd][maxIndex] = Math.max(nums[maxIndex] - deriveAnsMemo(minInd, maxIndex - 1, memo),
                        nums[minInd] - deriveAnsMemo(minInd + 1, maxIndex, memo))
            }
        }
        return memo[minInd][maxIndex]
    }

    return deriveAnsMemo(0, nums.size - 1, memoisedMutArray) >= 0
}