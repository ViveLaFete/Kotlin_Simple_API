package LeetCodeSolutions

fun frequencySort(s: String): String {
    val characterRepeatsMap: MutableMap<Char, Int> = mutableMapOf()
    for (c in s) {
        if (characterRepeatsMap.containsKey(c)) {
            characterRepeatsMap[c] = characterRepeatsMap[c]!! + 1
        }
        else {
            characterRepeatsMap[c] = 1
        }
    }
    //println(characterRepeatsMap)
    val sorted: List<Char> = characterRepeatsMap.keys
            .sortedByDescending { characterRepeatsMap[it] }
    return buildSortedString(sorted, characterRepeatsMap).toString()
}

private fun buildSortedString(sorted: List<Char>, characterRepeatsMap: MutableMap<Char, Int>): StringBuilder {
    val result = StringBuilder()
    for (c in sorted) {
        val repeats: Int? = characterRepeatsMap[c]
        for (i in 1..repeats!!) {
            result.append(c)
        }
    }
    return result
}